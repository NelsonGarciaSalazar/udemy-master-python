"""
    Exercise 9. Make a program that requests the user a number less the 100, if this is major than 100 get out
    the program
"""

number = 0

while number < 100:
    num = int(input("Try again enter the number: "))

    if num > 100:
        break
    else:
        print(f"You entered the number {num}")
