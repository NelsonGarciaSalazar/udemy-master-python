# Exercise_2. Write a script that show us for screen all numbers pairs from 1 to 120
number = 20

print(f"The numbers from 0 to {number} pairs are:")

for counter in range(0, number + 1):
    if (counter % 2) == 0:
        print(counter)
