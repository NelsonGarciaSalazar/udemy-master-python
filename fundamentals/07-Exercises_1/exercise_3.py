"""
    Exercise 3. Write a program that show the squatters (a number multiplicative for himself) of the firsts 60 primary
                natural numbers. Resolve with for and while
"""

numbers = 20

print("Solved exercise with FOR")

for counter in range(2, numbers + 1):
    print(f"The squatter of {counter} is {counter * counter}")


print("\nSolved exercise with WHILE")

counter = 2

while (counter > 1) and (counter <= numbers):
    print(f"The squatter of {counter} is {counter * counter}")
    counter += 1
 