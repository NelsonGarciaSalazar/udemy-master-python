"""
    Make a program that show us all numbers within two numbers given by user
"""

number1 = int(input("Digit the first number: "))
number2 = int(input("Digit the second number: "))
counter = str()
begin_number = number1

while number1 <= number2:
    counter += ", " + str(number1)
    number1 += 1

if number2 <= begin_number:
    print(f"The number1 {number1} is less or equal to {number2} that is incorrect")
else:
    print(f"The numbers within {begin_number} and {number2} are {counter}")
