"""
    Exercise 10. Enter the 5 students' score and count how many pass and fail
"""

counter = 1
number_pass = 0
number_fail = 0

students = int(input("How many students do you have: "))
print()

while counter <= students:
    score = float(input(f"Enter the score for student : {counter}: "))

    if 0 <= score <= 5:
        if score >= 3:
            number_pass += 1
        else:
            number_fail += 1

        counter += 1
    else:
        print("Invalid score. Please enter a score between 0 and 5.")


print(f"\nThe number of passing students is {number_pass}")
print(f"The number of failing students is {number_fail}")
