"""
    Exercise 6. Show all tables of multiplication from 1 to 10 and the title of each table
"""

for counter1 in range(1, 11):
    print(f"\nTable of multiplication of: {counter1}")
    for counter2 in range(1, 11):
        print(f"{counter1}\tx\t{counter2}\t=\t{counter1 * counter2}")


