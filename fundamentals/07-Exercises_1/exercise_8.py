"""
    Exercise 8. How much is the percentage (%) of an X number?
"""

number = int(input("Enter the number over that do you want calculate the percentage: "))
percentage = int(input(f"Enter the percentage that do you want calculate over {number}: "))

print(f"The {percentage} percentage over {number} is {number * percentage / 100}")
