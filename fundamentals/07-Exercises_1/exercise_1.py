"""
Exercise_1
    - Create variables one "Country" and "Continent"
    - Show her value for screen
    - Put the comment showing the tpe
"""

country = "Colombia"
continent = "American"

print(f"Country: {country} of continent: {continent}")

country_type = type(country)
continent_type = type(continent)

print(f"The variable country is of type: {country_type} and the tpe of continent is: {continent_type}")

