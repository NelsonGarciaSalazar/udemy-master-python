"""
    Exercise 7.  Make a program that show shows all the odd numbers, within two numbers that user choices
"""
number1 = int(input("Enter the first number: "))
number2 = int(input("Enter the second number: "))


def list_odd(num1, num2):
    odd_numbers = []
    for counter in range(num1, num2 + 1):
        if counter % 2 != 0:
            odd_numbers.append(counter)
    return odd_numbers


if number1 >= number2:
    print(f"The first number {number1} must be greater than the second number {number2}")
else:
    list_odd_numbers = list_odd(number1, number2)
    print(f"The list of odd numbers is: {list_odd_numbers}")
    