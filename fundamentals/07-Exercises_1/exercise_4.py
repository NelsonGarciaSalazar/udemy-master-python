"""
    Exercise_4. Requests two numbers to user and performance arithmetic operations
"""

number1 = int(input("Digit the first number: "))
number2 = int(input("Digit the second number: "))

print(f"The sum of {number1} + {number2} is {number1 + number2}")
print(f"The rest of {number1} - {number2} is {number1 - number2}")
print(f"The multiplication of {number1} * {number2} is {number1 * number2}")
print(f"The division of {number1} / {number2} is {number1 / number2}")
