from users import actions

user_actions = actions.Actions()


def show_menu():
    menu = """
            ------------ APP NOTES -------------
            Choice an option with a number:
                1. Register
                2. Login
                3. Exit
          """

    option = 0

    while option != 3:
        print(menu)

        option = int(input("Enter the number of your selection: "))

        if option == 1:
            user_actions.user_register()

        elif option == 2:
            user_actions.user_login()

        elif option == 3:
            print("Good Bye !!!")
            exit(0)
        else:
            print("Select a valid option")
