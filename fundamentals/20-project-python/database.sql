CREATE DATABASE IF NOT EXISTS MySQLPythonMaster;
use MySQLPythonMaster;

CREATE TABLE IF NOT EXISTS users(
    id          int(25) auto_increment not null,
    name        varchar(100),
    last_name   varchar(100),
    email       varchar(255),
    password    varchar(255) not null,
    date_log    date not null,
    CONSTRAINT pk_users PRIMARY KEY(id),
    CONSTRAINT uq_email UNIQUE(email)
)ENGINE=InnoDb;

CREATE TABLE IF NOT EXISTS notes(
    id          int(25) auto_increment not null,
    user_id     int(25) not null,
    title       varchar(255) not null,
    description MEDIUMTEXT,
    date_log    date not null,
    CONSTRAINT  pk_notes PRIMARY KEY(id),
    CONSTRAINT  fk_note_user FOREIGN KEY(user_id) REFERENCES users(id)
)ENGINE=InnoDb;
