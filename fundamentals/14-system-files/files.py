from io import open
import pathlib
import shutil
import os

# Create path absolute
path = str(pathlib.Path().absolute()) + "/file_text.txt"

# Open files with all permissions
file = open(path, "a+")

# Write in the file
file.write("\nThis is a text write from Python")

# Close the file
file.close()

# Open file with only read permission
file_read = open(path, "r")
content = file_read.read()
print(content)


# Converse the file in a list
file_list = open(path, "r")
file_list.readline()

for phrase in file_list:
    print(f"- {phrase}")

file_list.close()

# Copy file
original_path = str(pathlib.Path().absolute()) + "/file_text.txt"
new_path = str(pathlib.Path().absolute()) + "/file_text_copy.txt"
alternative_path = "../14-system-files/file_text_alternative.txt"

shutil.copyfile(original_path, alternative_path)

# Move or rename file
new_path_rename = str(pathlib.Path().absolute()) + "/file_text_rename.txt"
shutil.move(original_path, new_path_rename)

# Confirm if the file exist
print(os.path.abspath("./"))

if os.path.abspath(new_path):
    print("The file exist and the file file_text_copy.txt was delete")
    # DELETE file
    os.remove(new_path)
else:
    print("The file not exist")
