import os
import shutil

original_path = "./my_directory"
new_path = "./my_directory_copy"

# Create folder
if not os.path.isdir(original_path):
    os.mkdir(original_path)
    print(f"The directory {original_path} has been created")
else:
    print(f"The directory {original_path} already exist")

# Mover directory
shutil.move(original_path, new_path)
print(f"The directory {new_path} has been moved or replaced")

# Confirm the directory exist and delete
if os.path.exists(new_path):
    shutil.rmtree(new_path)
    print(f'The directory {new_path} has been deleted.')
else:
    print(f'The directory {new_path} not exist.')

# Create and list files inside of the directory
os.mkdir(original_path)

file1 = open("./my_directory/file.txt", "a+")
file1.write("This a comment inside the file")
file1 = open("./my_directory/file.txt", "r")
content_file = file1.read()
print(content_file)

file2 = open("./my_directory/hello.txt", "a+")

file1.close()
file2.close()

print("\n***Content of mt directory***")
content_directory = os.listdir(original_path)

for file in content_directory:
    print(f"File: {file}")
