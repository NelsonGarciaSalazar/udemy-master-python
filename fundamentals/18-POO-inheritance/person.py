"""
    Inheritance is the possibility of share attributes and methods within classes. And different classes inheritance the
    others
"""


# Class Father
class Person:
    def __init__(self, name, age):
        self.__name = name
        self.__age = age

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__name = value

    @property
    def age(self):
        return self.__age

    @age.setter
    def age(self, value):
        self.__age = value

    def greeting(self):
        return f"Hi, My name is {self.name} and have {self.age} years."
