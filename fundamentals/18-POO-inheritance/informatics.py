from person import Person


# Class son
class Informatics(Person):
    def __init__(self, name, age, language):  # Is optional but is good practices
        super().__init__(name, age)  # Is optional but is good practices
        self.__language = language

    @property
    def language(self):
        return self.__language

    @language.setter
    def language(self, value):
        self.__language = value

    def show_language(self):
        return f"I'm informatics and develop in {self.language}."
