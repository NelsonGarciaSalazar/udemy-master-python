from person import Person
from informatics import Informatics
from technical_network import TechnicalNetwork

person_instance = Person("Nelson", 43)
print(person_instance.greeting())

print("----------------------------")

informatics_instance = Informatics("Alisson", 19, "Python")
print(informatics_instance.greeting())
print(informatics_instance.show_language())

print("----------------------------")

# Modify attributes using setters
informatics_instance.name = "Elkin"
informatics_instance.age = 32
informatics_instance.language = "Java"

print(informatics_instance.greeting())
print(informatics_instance.show_language())

print("----------------------------")

technical_instance = TechnicalNetwork("CISCO", "Cristian", 36, "Bach")
print(technical_instance.greeting())
print(technical_instance.show_language())
print(technical_instance.show_certification())
