from informatics import Informatics


class TechnicalNetwork(Informatics):
    def __init__(self, certification, name, age, language):
        super().__init__(name, age, language)
        self.__certification = certification

    @property
    def certification(self):
        return self.__certification

    @certification.setter
    def certification(self, certification: str):
        self.__certification = certification

    def show_certification(self):
        return f"I am the certification in {self.certification}"
