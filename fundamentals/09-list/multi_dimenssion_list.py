contacts = [
    [
        "Nelson",
        "nelson@gmail.com"
    ],
    [
        "Liz",  # position[1][0]
        "liz@hotmail.com"
    ],
    [
        "Alisson",
        "alisson@gmail.com"
    ]
]

print(contacts[1][0])    # Liz

print("\n### Print all contacts ###\n")

for contact in contacts:
    for element in contact:
        if contact.index(element) == 0:
            print("Name: " + element)
        else:
            print("Mail: " + element)
    print("\n")
