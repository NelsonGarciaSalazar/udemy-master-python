"""
    Lists (arrays) with a set of variables that are stored under a variable and can be accessed using a numerical index
"""

print("\n### Declare a list ###")

movies = ["Batman", "Superman", "Spiderman"]
singers = list(("2pac","Drake", "Carlos"))  # Convert tuple to list
years = list(range(2020, 2025))
variate = ["Nelson", 30, 4.2, True]

print(movies)
print(singers)
print(years)
print(variate)

print("\n### Index ###")

print(movies[1])
print(movies[-2])

print(singers[1:3])
print(singers[0:1])
print(singers[0:2])

# get out all elements from a index
print(singers[1:])

print("\n### Change an element of the list ###")

movies[1] = "Gran torino"
print(movies)

print("\n### Append an element to list ###")

singers.append("Metallica")
print(singers)

new_movie = ""

while new_movie != "Stop":
    new_movie = input("\nEnter the new movie: ")

    if new_movie != "Stop":
        movies.append(new_movie)

print("\n### Delete an element ###")
# movies.remove("Stop")
del movies[0]
print(movies)


print("\n### Go through a list ###")

for movie in movies:
    print(movie)

print()
for singer in singers:
    print(f"{singer.index(singer)+1}. {singer}")

