singers = ["Shakira", "Juanes", "Vives"]
numbers = [0, 9, 8, 4, 5, 6, 2, 1]

print(singers)
print(numbers)

print("\nOrder list")

numbers.sort()
print(numbers)

print("\nAdd element")

singers.append("Julio")
print(singers)

singers.insert(1, "Bichota")
print(singers)


print("\nDelete element")
singers.pop(1)
print(singers)

singers.remove("Julio")
print(singers)

print("\nReverse the list")

print(numbers)
numbers.reverse()
print(numbers)

print("\nFind inside a list")
print("Shakira" in singers)     # True

print("\nCount elements")

print(singers)
print(len(singers))

print("\nCount how many repeat an element")
print(numbers)
print(numbers.count(1))
numbers.append(1)
print(numbers.count(1))


print("\nHow knw index of an element")
print(singers.index("Juanes"))

print("\nJoin listas")
singers.extend(numbers)
print(singers)

