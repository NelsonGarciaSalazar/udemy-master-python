class Car:
    # Constructor
    def __init__(self, make: str, model: str, year: int, color: str):
        self.__make = make
        self.__model = model
        self.__year = year
        self.__color = color

    # Static method to create a Car object
    @staticmethod
    def create_car(make: str, model: str, year: int, color: str) -> 'Car':
        return Car(make, model, year, color)

    # Define __str__ method for better string representation
    def __str__(self) -> str:
        return f"{self.__year} {self.__make} {self.__model} in {self.__color}"


# Llamada al method static from class
car1 = Car.create_car("Toyota", "Camry", 2020, "Red")

# Llamada al method static from an instance (although it's not necessary)
car_instance = Car("Ford", "Focus", 2019, "Blue")
car2 = car_instance.create_car("Honda", "Civic", 2018, "Blue")

print(car_instance)  # Create an instance from constructor
print(car1)  # Output: 2020 Toyota Camry in Red
print(car2)  # Output: 2018 Honda Civic in Blue

car1.__color = "Green"
print(f"\nDo not change the attribute with car1._color = 'Green': \n{car1}")
