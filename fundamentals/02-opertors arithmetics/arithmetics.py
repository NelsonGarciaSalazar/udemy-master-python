# Operator arithmetics

# allocation
age = 5
print(age)

age += 3
print(age)

age -= 3
print(age)

age = -2  # allocation the number -2
print(age)

age = +2  # allocation the number 2
print(age)

# Increment and decrement

# Tese no exist in Python
'''
    age--
    age++
    --age
    ++age
'''

# alternative
age = 1 + age
print(age)

age = age - 1
print(age)

print("***CALCULATOR***")
print(f"The result rest is: {5 - 3}")
print(f"The rest of division: {5 % 2}")
