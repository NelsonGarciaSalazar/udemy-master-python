import sqlite3

# Connection
connection = sqlite3.Connection('test.db')

# Create cursor
cursor = connection.cursor()

# Create sentence
cursor.execute("""
                CREATE TABLE IF NOT EXISTS products(
                   id INTEGER PRIMARY KEY AUTOINCREMENT,
                   title varchar(255),
                   description text,
                   price int(255)
               );
               """)

# Save Changes
connection.commit()

# Insert data
# cursor.execute("INSERT INTO products VALUES(null, 'First product', 'description of product', 550)")
# connection.commit()

# Delete records
cursor.execute("DELETE FROM products")
connection.commit()

# Insert many records to hit
products = [
    ("Laptop", "IBM", 4200),
    ("Mobile", "Apple", 1000),
    ("Table", "XIAOMI", 2000),
]

cursor.executemany("INSERT INTO products VALUES (null, ?, ?, ?)", products)
connection.commit()

# Update record
cursor.execute("UPDATE products SET price = 5000 WHERE id = 11")
connection.commit()

# List data
cursor.execute("SELECT * FROM products")
products = cursor.fetchall()

for product in products:
    print("Id: ", product[0])
    print("Title: ", product[1])
    print("Description: ", product[2])
    print("price: ", product[3])
    print("\n")

cursor.execute("SELECT * FROM products")
product = cursor.fetchone()
print(product)

# Close connection
connection.close()
