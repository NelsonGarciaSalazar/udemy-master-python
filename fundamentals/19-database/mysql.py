import pymysql

try:
    connection = pymysql.connect(
        host='localhost',
        user='root',
        password='root',
        database='MySQLPythonMaster'
    )

    with connection.cursor() as cursor:
        cursor.execute("SELECT VERSION()")
        version = cursor.fetchone()
        print("Versión de MySQL:", version)

        # Create Table
        cursor.execute("""
        CREATE TABLE IF NOT EXISTS cars(
            id      int(10) auto_increment not null,
            branch  varchar(40) not null,
            model   varchar(40) not null,
            price   float(10,2) not null,
            CONSTRAINT pk_cars PRIMARY KEY(id)
        )
        """)

        # Confirm created table car
        cursor.execute("SHOW TABLES")

        for table in cursor:
            print(table)

        # Insert a record
        cursor.execute("INSERT INTO cars VALUES(null, 'Opel', 'Astra', 100000)")
        connection.commit()

        # Insert several records
        cars = [
            ('Seat', 'Arona', 120000),
            ('Renault', 'Clio', 90000),
            ('Citroen', 'Class C', 80000)

        ]

        cursor.executemany("INSERT INTO cars VALUES(null, %s, %s, %s)", cars)
        connection.commit()

        # Select records
        cursor.execute("SELECT * FROM cars")
        result = cursor.fetchall()

        print("--- Print all Cars ---")
        for car in result:
            print(car)
            print(f"Print several values: {car[1]} : {car[3]}")

        print(f"\nPrint only one: {result[1]}")

        # Delete records
        cursor.execute("DELETE FROM cars WHERE branch = 'Opel' and id >= 2")
        connection.commit()

        print(cursor.rowcount, "Deletes !!")

        # Update records
        cursor.execute("UPDATE cars SET branch = 'Ferrari' WHERE branch = 'Opel'")
        connection.commit()

        print(cursor.rowcount, "Updates !!")

except pymysql.MySQLError as e:
    print("Error to connect to MySQL", e)

finally:
    if connection:
        connection.close()
        print("\nThe connection to MySQL has been closed")
