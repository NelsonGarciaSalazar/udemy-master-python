# Variables
nothing = None
string = "Hello python"
integer = 24
var_float = 4.2
boolean = True
list_numbers = [10, 20, 30]
list_string = [10, "twenty", 30]
tuple_no_change = ("master", "en", "python", 4)  # like a constant
dictionary = {
    "name": "Nelson",
    "last_name": "Garcia"
}
rango = range(9)
data_byte = b"testing"

# Print variable
print(nothing)
print(tuple_no_change)
print(dictionary)
print(rango)
print(data_byte)

# Show type of variable
print(type(nothing))
print(type(string))
print(type(integer))
print(type(var_float))
print(type(boolean))
print(type(list_numbers))
print(type(list_string))
print(type(tuple_no_change))
print(type(dictionary))
print(type(rango))
print(type(data_byte))


# Convert type of variable - only with functions
text = "Hello I'm a text"

number = str(77)  # 77 to "77"
print(type(number))

number = int(77)  # "77" to 77
print(type(number))

number = float(77)  # 77 to 77.0
print(type(number))

print(f"{text} {number}")
