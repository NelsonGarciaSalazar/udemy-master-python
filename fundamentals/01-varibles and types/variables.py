"""
Una variable es un contenedor de información, dentro guardará un dato, se pueden crear
muchas variables y que cada una tenga un dato distinto
"""

# Concatenar
name = "Nelson"
last_name = "Garcia"

print(name + " - " + last_name)
print(f"{name} - {last_name}")
print(name, last_name)
print("nombre {} apellido {}".format(name, last_name))

