print("#### BUCLE ####")

# --------------------------------------- #
print("\n#### FOR ####")

counter = 0
result = 0

for counter in range(2, 4):
    print("I am going for number:" + str(counter))
    result += counter

print(f"The result of sum the interactions is: {result}")

# --------------------------------------- #
print("\n#### Multiplication Tables ####")

# number_user = int(input("From the number do you want the table?: "))
number_user = 2

for number_table in range(0, 11):

    if number_user == 13:
        print(f"Forbidden number {number_user}")
        break

    print(f"{number_table}\tx\t{number_user}\t=\t{number_table * number_user}")
else:
    print("The table finished")

# --------------------------------------- #
print("\n#### Bucle While ####")

counter = 0

while counter <= 10:
    print(f"I am going for number: {counter}")
    counter += 1


# --------------------------------------- #
counter = 0
show_number = str(0)

while counter <= 10:
    show_number = show_number + ", " + str(counter)
    counter += 1

print(f"\nList of numbers: {show_number}")

# --------------------------------------- #
print("\n#### Multiplication Table with WHILE ####")
number_user = 2
counter = 0

while counter <= 10:
    print(f"{counter}\tx\t{number_user}\t=\t{counter * number_user}")
    counter += 1
else:
    print("The table finished")



