print("### Detect typing ###")

name = "Nelson Garcia"

confirm = isinstance(name, str)

if confirm:
    print("The variable is string")
else:
    print("The variable is not string")


print("\n### Spaces Clean ####")

phrase = "       my content "

print(phrase)
print(phrase.strip())


print("\n#### Delete variable ###")
year = 2024
print(year)
# del year
print(year)


print("\n### Confirm if variable is empy ###")
car = "honda"

if len(car) <= 0:
    print("The variable is empy")
else:
    print(len(car))

print("\n### find characters or words in the variable ###")
phrase = "The life is beautiful"
print(phrase.find("life"))

print("\n### Replace words in the string variable ###")
new_phrase = phrase.replace("life", "bike")
print(new_phrase)

print("\n### CConvert capital letter to lower letter ###")
print(new_phrase.upper())
print(new_phrase.lower())
print(new_phrase.capitalize())
print(new_phrase.casefold())    # more complete than lower
