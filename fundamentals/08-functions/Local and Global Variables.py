"""
    Global Variable: This is available for all program
    Local Variable: This is available only inside of the function
"""

sentence = "This is a Global variable"


def function():
    print("\nInside the function")
    sentence = "This is a Local Variable"
    print(sentence)


print(sentence)

function()


print("\nConvert a Local variable to Global variable")


def convert_local_to_global_variable():

    global local_text
    local_text = "This is declared inside the function"

    print(f"Print from inside the function: {local_text}")


convert_local_to_global_variable()

print(f"\nPrint from outside the function: {local_text}")
