print("Basic structure")


def show_names():
    print("Nelson Developer")


show_names()


def show_names_to_request(name):
    print(name)


show_names_to_request("Liz")


def sum_two_numbers(num1, num2):
    return num1 + num2


sum_two_numbers(2, 6)

print("\n#### Optional parameters ####")


def get_employee(name, dni=None):
    print("\nEmployee")
    print(name)

    if dni is not None:
        print(dni)


get_employee("Nelson García", 123)

get_employee("Liz Ariza")

print("\n#### Call a function inside other function #####")


def get_name(name):
    return name


def get_last_name(last_name):
    return last_name


def give_back_all(the_name, the_last_name):
    text = get_name(the_name) + " " + get_last_name(the_last_name)
    return text


print(give_back_all("Nelson", "Garcia"))

print("\n#### LAMBDA FUNCTION #####")
print(" lamda arguments: expression")


do_sum = lambda a, b: a + b


print("The sum is: " + str(do_sum(2, 4)))

print("\nCommon Functions map (for), filter(if), reduce (iterative accumulate), sorted")

# Using map for duplicate each number in a list
numbers = [1, 2, 3, 4]
dobles = list(map(lambda x: x * 2, numbers))
print(dobles)  # Output: [2, 4, 6, 8]

# Using filter for filter numbers pairs
pares = list(filter(lambda x: x % 2 == 0, numbers))
print(pares)  # Output: [2, 4]

# Using reduce for sum all the numbers in a list
from functools import reduce
suma_total = reduce(lambda x, y: x + y, numbers)
print(suma_total)  # Output: 10


# Order a list of tuples by the second value
pares = [(1, 2), (3, 1), (5, 4)]
ordered = sorted(pares, key=lambda x: x[1])
print(ordered)  # Output: [(3, 1), (1, 2), (5, 4)]


print("\nOther example of REDUCE")

products = [
    {'name': 'Product 1', 'Price': 100},
    {'name': 'Product 2', 'Price': 200},
    {'name': 'Product 3', 'Price': 150}
]

Price_total = reduce(lambda x, y: x + y['Price'], products, 0)
print(Price_total)  # Output: 450

"""
    x es el acumulador que empieza en 0.
    y es cada diccionario en la lista.
    x + y['precio'] suma el precio del producto actual al acumulador.
"""


print("\nOther example of SORTED")

students = [
    {'name': 'Juan', 'nota': 85},
    {'name': 'Ana', 'nota': 92},
    {'name': 'Pedro', 'nota': 78},
    {'name': 'Marta', 'nota': 90}
]

# Order estudents por su nota de forma desc
students_ordered = sorted(students, key=lambda x: x['nota'], reverse=True)
print(students_ordered)
# Output: [{'name': 'Ana', 'nota': 92}, {'name': 'Marta', 'nota': 90}, {'name': 'Juan', 'nota': 85}, {'name': 'Pedro', 'nota': 78}]

"""
    key=lambda x: x['nota'] especifica que queremos ordenar por la clave 'nota' de cada diccionario.
    reverse=True ordena de forma descendente.
"""
