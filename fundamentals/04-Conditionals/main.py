print("##### Conditional IF #########")

# -------------------------------------------
print("\n##### Example 1 #####")

color = "red"

if color == "red":
    print("Good!!!")
    print("The color is red")
else:
    print("The color isn't red")

print("\n##### Example 2 Operators #####")

year = 2020

if year >= 2021:
    print(f"The year {year} is equal or mayor to 2021")
else:
    print(f"The year {year} is less to 2021")

# -------------------------------------------
print("\n##### Example 2 If Nested #####")

name = "Nelson Garcia"
city = "Bogotá"
continent = "American"
age = 43
lawful_age = 18

if age >= lawful_age:
    print(f"{name} is lawful of age")

    if continent != "American":
        print("The user isn't American")
    else:
        print(f"Tne user is American and of {city}")

else:
    print(f"{name} don't have the lawful of age")

# -------------------------------------------
print("\n##### Example 3 If and elif this replace Switch instruction #####")

day = 5

if day == 1:
    print("The day is Monday")
elif day == 2:
    print("The day is Tuesday")
elif day == 3:
    print("The day is Wednesday")
elif day == 4:
    print("The day is Thursday")
elif day == 5:
    print("The day is Friday")
elif day == 6:
    print("The day is Saturday")
elif day == 7:
    print("The day is Sunday")
else:
    print("The day isn't between 1 to 7")

# -------------------------------------------
print("\n##### Example 5 Logic Operators #####")

minimum_age = 18
maximum_age = 62
official_age = 20

if (official_age < maximum_age) & (official_age > minimum_age):
    print("Is of working age")
else:
    print("Isn't of working age")

# -------------------------------------------
print("\n##### Example 6 Logic Operators #####")

'''
and     & 
or      |
not     !
'''

country = "German"
if country == "Mexico" or country == "Spain" or country == "Colombia":
    print(f"{country} is a country to talk spain")
else:
    print(f"{country} isn't a country to talk spain")

country = "Russia"
if not (country == "Mexico" or country == "Spain" or country == "Colombia"):
    print(f"{country} isn't a country to talk spain")
else:
    print(f"{country} is a country to talk spain")

country = "Russia"
if country != "Mexico" or country != "Spain" or country != "Colombia":
    print(f"{country} isn't a country to talk spain")
else:
    print(f"{country} is a country to talk spain")
