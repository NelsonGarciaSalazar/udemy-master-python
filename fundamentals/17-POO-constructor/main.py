from car import Car

# Abstraction the model Car class
car1 = Car("Ford", "Fiesta", 2024, "Green")
car2 = Car("Seat", "Arona", 2019, "Grey")

print(car1)
print(car1.color)
print(car1.get_info())
print(car2.get_info())

# Visibility
car1.model = "Focus"
print(car1.model)
