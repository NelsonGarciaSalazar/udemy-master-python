class Car:
    # Constructor
    def __init__(self, make: str, model: str, year: int, color: str):
        self.__make = make
        self.__model = model
        self.__year = year
        self.__color = color

    # Property for make
    @property
    def make(self) -> str:
        return self.__make

    @make.setter
    def make(self, make: str) -> None:
        self.__make = make

    # Property for model
    @property
    def model(self) -> str:
        return self.__model

    @model.setter
    def model(self, model: str) -> None:
        self.__model = model

    # Property for year
    @property
    def year(self) -> int:
        return self.__year

    @year.setter
    def year(self, year: int) -> None:
        self.__year = year

    # Property for color
    @property
    def color(self) -> str:
        return self.__color

    @color.setter
    def color(self, color: str) -> None:
        self.__color = color

    # String representation
    def __str__(self) -> str:
        return f"{self.__year} {self.__make} {self.__model} in {self.__color}"

    def get_info(self) -> str:
        return f"""
                These are attributes of Car class:
                Branch:\t{self.__make}
                Model:\t{self.__model}
                Year:\t{self.__year}
                Color:\t{self.__color}
               """
