"""
    This is a type of data, for have a collection of values, but don't have index and order
"""

person = {
    "Victor",
    "Manolo",
    "Francisco"
}

print(type(person))
print(person)   # show the elements in disorder

print("\n add element")

person.add("Paco")
person.remove("Francisco")

print(person) 
