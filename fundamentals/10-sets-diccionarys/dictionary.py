"""
    Dictionary:
    is a type of data that storage a group of data.
    The format is key: value
    Is similar to an array associate or an object JSON
"""

person = {
    "name": "Nelson",
    "lastname": "Garcia",
    "Mail": "nelsong.salazar@gmail.com"
}

print(type(person))
print(person)
print(person["lastname"])


print("\n#### List of the dictionary")
contacts = [
    {
        "name": "Nelson",
        "Mail": "nelsong.salazar@gmail.com"
    },
    {
        "name": "Alisson",
        "Mail": "alisson@gmail.com"
    },
    {
        "name": "Nelson",
        "Mail": "elkin@gmail.com"
    }
]

print(contacts)
print(contacts[0]["name"])
contacts[0]["name"] = "Peter"
print(contacts[0]["name"])

print("\n### ist of Contacts ###")
print("-------------------------------------")

for contact in contacts:
    print(f"Name of Contact: {contact['name']}")
    print(f"Mail of Contact: {contact['Mail']}")
    print("-------------------------------------")
