"""
    Exercise 5.
    Create a DICTIONARY with the video games table with three categories each one with three games and show it
"""

# Defined the category and the games in each one

video_games = {
    "Adventure": ["The Legend of Zelda: Breath of the Wild", "Uncharted 4: A Thief's End", "The Witcher 3: Wild Hunt"],
    "Action": ["God of War", "Doom Eternal", "Call of Duty: Modern Warfare"],
    "Rol": ["Final Fantasy VII Remake", "Persona 5", "Elden Ring"]
}

# Print the table
for category, games in video_games.items():
    print(f"Category: {category}")
    for game in games:
        print(f" - {game}")
    print()
