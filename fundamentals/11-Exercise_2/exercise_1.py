"""
    Exercise 1: Make a program that has a list of 8 integer numbers and do the followed:
        1. Iterate through the list and show it
        2. Make a function that Iterate the list of numbers and return a string
        3. Order the list and show it
        4. Show the length
        5. Find some element (That the user request for keyboard)
"""

print("\nPrint list")
numbers = [12, 23, 34, 45, 56, 32, 21, 50]
print(numbers)

print("\nconvert from list to String and show it")


def show_list(num_list):

    string_numbers = ""

    for number in num_list:
        string_numbers += str(number) + ", "

    return string_numbers


print(show_list(numbers))


print ("\nOrder list and show it")

numbers.sort()
print(numbers)

print("\nShow the length")
print(len(numbers))

found = False

while not found:
    request = int(input("\nEnter the number to find in the list: "))

    if request in numbers:
        print(f"The number {request} is fond in the position: {numbers.index(request)}")
        found = True
    else:
        print(f"The number {request} don't exist in the numbers list")
