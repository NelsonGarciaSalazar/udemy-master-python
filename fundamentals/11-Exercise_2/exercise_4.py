"""
    Exercise 4.
    Make a program that has four variables, a list, a string, an integer and boolean and prints a message
    according to the data type. User functions
"""

my_list = ["hello world", 12]
title = "master in python"
number = 123
decision = True


def show_data_type(data, class_type):

    test = isinstance(data, class_type)
    result = ""

    if test:
        result = f"This data is the type {type(data)}"
    else:
        result = "The type of sata doesn't correct"

    return result


print(show_data_type(my_list, list))
print(show_data_type(title, str))
print(show_data_type(number, int))
print(show_data_type(decision, bool))
