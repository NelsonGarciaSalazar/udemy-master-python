print("*** Example using RAISE ***")

class PasswordTooShortError(Exception):
    """Excepción lanzada cuando la contraseña es demasiado corta."""
    pass


class PasswordMissingCharacterError(Exception):
    """Excepción lanzada cuando la contraseña no contiene al menos un carácter especial."""
    pass


def verificar_contraseña(contraseña):
    if len(contraseña) < 8:
        raise PasswordTooShortError("La contraseña debe tener al menos 8 caracteres.")

    caracteres_especiales = "!@#$%^&*()-+"
    if not any(char in caracteres_especiales for char in contraseña):
        raise PasswordMissingCharacterError("La contraseña debe contener al menos un carácter especial.")

    return True


# Uso de la función con manejo de excepciones
try:
    contraseña = "abc123"
    verificar_contraseña(contraseña)
except PasswordTooShortError as e:
    print(f"Error de contraseña: {e}")
except PasswordMissingCharacterError as e:
    print(f"Error de contraseña: {e}")
else:
    print("La contraseña es válida")
finally:
    print("Verificación de contraseña completada")
