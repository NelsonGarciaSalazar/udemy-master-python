"""
    El manejo de excepciones en Python se realiza utilizando bloques try, except, else y finally. A continuación se detalla cada uno de ellos:

    try: Contiene el código que puede generar una excepción.
    except: Bloque que maneja la excepción si ocurre. Puedes especificar el tipo de excepción a manejar.
    else: Se ejecuta si no se generó ninguna excepción en el bloque try.
    finally: Se ejecuta siempre, ocurra o no una excepción, y se usa para liberar recursos o realizar acciones de limpieza.
"""

try:
    # Código que puede generar una excepción
    resultado = 10 / 0
except ZeroDivisionError as e:
    # Manejo de la excepción específica
    print(f"Error: {e}")
else:
    # Código que se ejecuta si no ocurre ninguna excepción
    print("La división fue exitosa")
finally:
    # Código que se ejecuta siempre
    print("Operación terminada")

"""
    Manejo de múltiples excepciones
    Puedes manejar diferentes tipos de excepciones de forma separada:
"""

try:
    # Código que puede generar una excepción
    resultado = int("Hola")
except ZeroDivisionError:
    # Manejo de la excepción ZeroDivisionError
    print("No se puede dividir por cero")
except ValueError:
    # Manejo de la excepción ValueError
    print("Error de valor: No se pudo convertir a entero")

"""
    Capturar todas las excepciones
    Es posible  capturar todas las excepciones, aunque no es una práctica recomendada porque puede ocultar errores
    inesperados:
"""

try:
    # Código que puede generar una excepción
    resultado = 10 / 0
except Exception as e:
    # Manejo de cualquier tipo de excepción
    print(f"Se produjo un error: {e}")

"""
    Lanzar excepciones Puedes lanzar excepciones manualmente utilizando la palabra clave raise:
"""

def dividir(a, b):
    if b == 0:
        raise ValueError("El divisor no puede ser cero")
    return a / b

try:
    resultado = dividir(10, 0)
except ValueError as e:
    print(f"Error: {e}")
