"""
    Modules are functions or features already made to uses
    The list of modules for Python It can consult here:
    https://docs.python.org/3/py-modindex.html

    Also we can create our modules
"""

# several ways to import the modules

# import my_module
# print(my_module.hello_word("Nelson Garcia"))

# from my_module import calculator
# print(calculator(2, 4))

from my_module import *

print(hello_word("Nelson Garcia"))
print(calculator(2, 4))

# module of date yyyy-mm-dd
import datetime

print("This module and function date.today() print the current date: " + str(datetime.date.today()))

# This function print the complete date yyyy-mm-dd hh:mm:ss
complete_date = datetime.datetime.now()
print(f"This function print the date complete: {complete_date}")

# I can extract parts of the complete date
print(f"Extract year: {complete_date.year}")
print(f"Extract month: {complete_date.month}")
print(f"Extract day: {complete_date.day}")

# customize date
customize_date = complete_date.strftime("%d/%m/%Y, %H:%M:%S")
print(f"This is my customize date: {customize_date}")

# Generate the timestamp that is useful for calculates
print(f"The time() date: {datetime.datetime.now().time()}")
print(f"The timestamp() date: {datetime.datetime.now().timestamp()}")
